export PATH="${PATH}:/bin"
export PATH="${PATH}:/sbin"
export PATH="${PATH}:/usr/bin"
export PATH="${PATH}:/usr/sbin"

current_host=$(hostname -s)
if [ $current_host == "asus" ]; then
    echo "Using asus host"
elif [ $current_host == "homesrv" ]; then
    source /release/wq/prod/bash_profile
    export PATH="${PATH}:/home/bogdan/Applications/miniconda3/envs/visa_warner/bin"
    export PATH="${PATH}:/home/bogdan/Applications/miniconda3/condabin"
    export PYTHONPATH="${PYTHONPATH}:/home/bogdan/Documents/visa_warner"
    export PYTHONPATH="${PYTHONPATH}:/home/bogdan/Applications/miniconda3/envs/visa_warner/lib/python3.7/site-packages"
else
    echo "Unknown host"
fi
