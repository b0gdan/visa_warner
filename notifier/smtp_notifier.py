import ssl
from loguru import logger
import smtplib


def send_smtp_message(smtp_config, recipient, subject, message_body):
    """
    Sends email to notify about updates
    Args:
        recipient (str):
        smtp_config (dict):
        subject (str): Subject for the email
        message_body (str): Main text of the email
    """
    try:
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_config['smtp_host'], smtp_config['smtp_port'], context=context) as server:
            server.login(smtp_config['smtp_user'], smtp_config['smtp_pass'])
            server.sendmail(smtp_config['smtp_user'],
                            recipient,
                            f"Subject: {subject}\n\n{message_body}")
            server.quit()
    except Exception as smtp_err:
        logger.error(smtp_err)