#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Send updates on visa status
"""
import logging
from datetime import datetime
import json
from os import path
import time
import urllib.request

from bs4 import BeautifulSoup
from loguru import logger

# from notifier.smtp_notifier import send_smtp_message
from notifier.xmpp_notifier import send_xmpp_message

# from visa_mock_data import get_actual_visas_state_mock
# from pprint import pprint


class VisaWarner:
    """
    Send updates on visa status
    """

    def __init__(self):
        """
        Initialize main class vars
        """
        self.xmpp_recipient = ""
        self.xmpp_config = {}
        self.my_visa_id = 12345
        self.email_recipient = ""
        self.smtp_config = {}
        self.notifications_frequency = 15  # Notify about status every 15th minute

    def get_actual_visas_state(self):
        """
        Download data from web site
        Returns (dict): dict with list of ready visas

        """
        try:
            # Get HTML text of the webpage
            file_pipe = urllib.request.urlopen(
                "https://germania.diplo.de/stpe-abholung"
            )
            my_bytes = file_pipe.read()
            html_doc = my_bytes.decode("utf8")
            file_pipe.close()
            soup = BeautifulSoup(html_doc, "html.parser")
            # Find necessary div with visas' IDs
            approved_visas_tags = soup.select_one(
                "div#item-1670270-0-panel > div > div > p:nth-child(2)"
            ).contents
            # Parse IDs into list
            approved_visas = {"approved_visas": []}
            today = datetime.now()
            for tag in approved_visas_tags:
                if str(tag) != "<br/>":
                    approved_visas["approved_visas"].append(
                        {
                            "date": today.strftime("%Y.%m.%d %H:%M:%S"),
                            "visa_id": str(tag),
                        }
                    )
            return approved_visas
        except Exception as download_err:
            err_msg = f"Failed to download council page! {download_err}"
            # send_smtp_message(
            #     self.smtp_config, self.email_recipient, "Visa status", err_msg
            # )
            send_xmpp_message(
                xmpp_config=self.xmpp_config,
                recipient=self.xmpp_recipient,
                subject="Visa status",
                message=err_msg,
            )

            logger.error(err_msg)
            return None

    def update_log_file(self, new_approved_visas):
        """
        Updates log file with "cache" of the history with visas
        Args:
            new_approved_visas:
        """
        try:
            # Define path where log file with IDs should be stored
            visas_log_path = path.join(
                path.realpath(__file__).replace(
                    path.basename(path.realpath(__file__)), ""
                ),
                "visas_log.json",
            )
            # If it doesn't exists create it with fresh data from the webpage
            if not path.isfile(visas_log_path):
                with open(visas_log_path, "w") as file_pipe:
                    json.dump(new_approved_visas, file_pipe, indent=4)
            # Now open this log
            with open(visas_log_path) as visas_log:
                visas_log_dict = json.load(visas_log)
            # Get old IDs in a form of list
            visas_log_ids = [
                int(d["visa_id"]) for d in visas_log_dict["approved_visas"]
            ]
            # Get new IDs in a form of list
            new_approved_visas_ids = [
                int(d["visa_id"]) for d in new_approved_visas["approved_visas"]
            ]
            # Do notification about IDs
            today = datetime.now()
            are_there_any_updates = False
            for visa_id in new_approved_visas_ids:
                if visa_id not in visas_log_ids:
                    tmp_id = {
                        "date": today.strftime("%Y.%m.%d %H:%M:%S"),
                        "visa_id": str(visa_id),
                    }
                    visas_log_dict["approved_visas"].append(tmp_id)
                    # Make a notification on any update
                    mail_subject = "Visa status: updates"
                    mail_message = (
                        "I found that new id was appeared "
                        f"{tmp_id['visa_id']}\nOn the following date: {tmp_id['date']}"
                    )
                    # Notify for my visa ID
                    if int(visa_id) == self.my_visa_id:
                        mail_subject = "Visa status: cool updates"
                        mail_message = (
                            f"Yeeeeeeah! Wiiiin! {tmp_id['visa_id']}\nOn the "
                            f"following date: {tmp_id['date']}"
                        )
                    # send_smtp_message(
                    #     self.smtp_config,
                    #     self.email_recipient,
                    #     mail_subject,
                    #     mail_message,
                    # )
                    send_xmpp_message(
                        self.xmpp_config,
                        self.xmpp_recipient,
                        mail_subject,
                        mail_message,
                    )
                    are_there_any_updates = True
            # Notify every fifteenth minute if there is no any update
            if not are_there_any_updates:
                if int(today.strftime("%M")) % self.notifications_frequency == 0:
                    # send_smtp_message(
                    #     self.smtp_config,
                    #     self.email_recipient,
                    #     "Visa status: silence",
                    #     "No any updates!",
                    # )
                    send_xmpp_message(
                        self.xmpp_config,
                        self.xmpp_recipient,
                        "Visa status: silence",
                        "No any updates!",
                    )
            # Update log file with new IDs
            with open(visas_log_path, "w") as file_pipe:
                json.dump(visas_log_dict, file_pipe, indent=4)
        except Exception as update_err:
            logger.error(f"Failed to download council page! {update_err}")
            # send_smtp_message(
            #     self.smtp_config,
            #     self.email_recipient,
            #     "Visa status",
            #     f"Failed to download council page! {update_err}",
            # )
            send_xmpp_message(
                self.xmpp_config,
                self.xmpp_recipient,
                "Visa status",
                f"Failed to download council page! {update_err}",
            )


def main():
    """
    Main logic of the script. Every 5 minutes fetch site.
    Every fifteen minute send state via email.
    """
    # Get credentials and config
    credentials_path = path.join(
        path.realpath(__file__).replace(path.basename(path.realpath(__file__)), ""),
        "credentials.json",
    )
    if not path.isfile(credentials_path):
        logger.error(f"Please specify credentials and config to the script in the following file: {credentials_path}")
        exit(1)
    with open(credentials_path) as visas_log:
        config = json.load(visas_log)
    # Setup logging.
    logging.basicConfig(level=config["logging_level"], format="%(levelname)-8s %(message)s")
    # Apply config
    visa_worker = VisaWarner()
    visa_worker.smtp_config = {
        "smtp_host": config["smtp_host"],
        "smtp_user": config["smtp_user"],
        "smtp_port": config["smtp_port"],
        "smtp_pass": config["smtp_pass"],
    }
    visa_worker.my_visa_id = config["my_visa_id"]
    visa_worker.notifications_frequency = config["notifications_frequency"]
    visa_worker.email_recipient = config["email_recipient"]
    visa_worker.xmpp_config = {
        "xmpp_jid": config["xmpp_jid"],
        "xmpp_pass": config["xmpp_pass"],
    }
    visa_worker.xmpp_recipient = config["jabber_recipient"]
    # Do main logic
    logger.debug("executing get_actual_visas_state")
    approved_visas = visa_worker.get_actual_visas_state()
    # approved_visas = get_actual_visas_state_mock()
    if approved_visas:
        logger.debug("executing update_log_file")
        visa_worker.update_log_file(approved_visas)
    else:
        logger.error("Failed to download council page!")


if __name__ == "__main__":
    main()
